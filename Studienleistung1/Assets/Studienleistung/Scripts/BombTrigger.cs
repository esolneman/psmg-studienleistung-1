﻿using UnityEngine;
using System.Collections;

public class BombTrigger : MonoBehaviour {

	private int bombDamage = 2;
	private int currentHealthPoint;
	private Collider bombCollider;
	private Renderer bombRenderer;
	private AudioSource audioBombExplosion;


	void OnTriggerEnter(Collider collision)
	{	
		if(collision.gameObject.name == "Player") {
			audioBombExplosion.Play ();
			deactivateBomb ();
			PlayerManager playerManager = collision.gameObject.GetComponent<PlayerManager>();
			//calculates health point after bomb explosion
			currentHealthPoint = playerManager.changeHealthPoint;
			playerManager.changeHealthPoint = currentHealthPoint - bombDamage;
		}
	}

	//hide the bomb and deactivate it by disable bomb collider
	private void deactivateBomb() {
		bombCollider.enabled = false;
		bombRenderer.enabled = false;
	}

	//show the bomb again and activate it
	public void activateBomb() {
		bombCollider.enabled = true;
		bombRenderer.enabled = true;
	}

	//initialization of bomb collider , bomb renderer and audio source 
	void Start () {
		bombCollider = gameObject.GetComponent<SphereCollider> ();
		bombRenderer = gameObject.GetComponent<Renderer> ();
		audioBombExplosion = GetComponent<AudioSource> ();
	}
}
