﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CanvasManager : MonoBehaviour {

	private string healthPointsString = "Health Points ";
	private Text healthPointText;
	private string timeString = "Time: ";
	private Text timeCounterText;
	private GameObject winText;
	private GameObject lostText; 
	private GameObject timeTextObject;
	private Text timeText;
	private float currentTimer;
	private string timeGoalString = "It took: ";
	private string secondsString = "seconds";
	private int waitSeconds = 5;
	private float startTextTimer;
	private Button openDoorButton;
	private int currentHealthPoints;
	private GameObject openDoorGameObject;
	private Door actualDoor;

	// initialization
	void Start () {
		healthPointText = GameObject.Find ("HealthPointUI").GetComponent<Text> ();
		timeCounterText = GameObject.Find ("TimeInfo").GetComponent<Text> ();
		timeText = GameObject.Find ("Canvas/TimeFinished").GetComponent<Text> ();
		currentTimer = 0.0f;
		winText = GameObject.Find ("WinInfo");
		lostText = GameObject.Find ("LostInfo");
		winText.GetComponent<Text> ().enabled = false;
		lostText.GetComponent<Text> ().enabled = false;
		timeTextObject = GameObject.Find("TimeFinished");
		timeTextObject.GetComponent<Text> ().enabled = false;
		initButton ();
	}

	private void initButton() {
		openDoorGameObject = GameObject.Find ("OpenDoorButton");
		openDoorButton = GameObject.Find ("OpenDoorButton").GetComponent<Button>();
		openDoorGameObject.SetActive (false);
	}

	void openDoorButtonListener(Door actualDoor) {
		actualDoor.changeDoorStatus = true;
	}

	//called if player walks in door trigger
	//show the button to open the current door
	public void showOpenDoorButton(Door actualDoor) {
		openDoorGameObject.SetActive (true);
		this.actualDoor = actualDoor;
		//add clickListener to the button
		openDoorButton.onClick.AddListener (() => {
			openDoorButtonListener (actualDoor);
		});
	}

	public void hideOpenDoorButton() {
		openDoorGameObject.SetActive (false);
	}

	public void changeHealthUI(int currentHealthPoints) {
		this.currentHealthPoints = currentHealthPoints;
		healthPointText.text = healthPointsString + currentHealthPoints;
	}

	public void showLostUI() {
		startTextTimer = Time.deltaTime;
		lostText.GetComponent<Text> ().enabled = true;
		showFinishedTime ();
	}

	public void showWinUI(){
		startTextTimer = Time.deltaTime;
		winText.GetComponent<Text> ().enabled = true;
		showFinishedTime ();
	}

	//show the time the player need to finish the game
	//start wait for 5 seconds, before the gamme will be restarted
	private void showFinishedTime () {
		float secsToWait = 5.0f;
		timeTextObject.GetComponent<Text> ().enabled = true;
		timeText.text = timeGoalString + currentTimer + secondsString;
		StartCoroutine (Wait());
	}

	IEnumerator Wait(){
		yield return new WaitForSeconds (5);
		closeTexts();
		GameObject.Find ("GameManagerObject").GetComponent<GameManager> ().resetGame ();
	}

	//hide game end texts
	private void closeTexts() {
		timeTextObject.GetComponent<Text> ().enabled = false;
		winText.GetComponent<Text> ().enabled = false;
		lostText.GetComponent<Text> ().enabled = false;
	}

	public void startTimeNew() {
		currentTimer = 0.0f; 
	}

	// Update is called once per frame
	void Update () {
		currentTimer += Time.deltaTime;
		timeCounterText.text = timeString + currentTimer;
	}
}
