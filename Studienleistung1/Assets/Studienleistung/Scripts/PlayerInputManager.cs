﻿using UnityEngine;
using System.Collections;

public class PlayerInputManager : MonoBehaviour {

	public float rotationSpeed = 50f;
	public float speedBoost = 15;

	private string inputMovementAxisName;
	private string inputRotationAxisName; 

	private float inputMovement;
	private float inputRotation;

	private Rigidbody rigidbody; 

	public GameObject player;

	/// Move the Player Forward or Backward
	private void Move()
	{
		inputMovement = Input.GetAxis(inputMovementAxisName);
		Vector3 movement = transform.forward * speedBoost * inputMovement * Time.deltaTime;
		rigidbody.MovePosition(rigidbody.position + movement);
	}
		
	// rotate the Player
	private void Rotate()
	{
		inputRotation = Input.GetAxis(inputRotationAxisName);
		float turn = inputRotation * Time.deltaTime*rotationSpeed;
		Quaternion turnRotation = Quaternion.Euler(0f,turn * 50f,0f);
		rigidbody.MoveRotation(rigidbody.rotation * turnRotation);
	}

	//set the player to a new location
	public void changePlayerLocation(Vector3 newLocation) {
		rigidbody.MovePosition (newLocation);
	}
		
	// update the Inputs of the Player per Step 
	void FixedUpdate () {
		Move();
		Rotate();
	}

	// initialization
	public void Init()
	{
		inputMovementAxisName = "Vertical";
		inputRotationAxisName = "Horizontal"; 
		rigidbody = GetComponent<Rigidbody>(); 
	}
		
}
