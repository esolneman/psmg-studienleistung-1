﻿using UnityEngine;
using System.Collections;

public class DoorTrigger : MonoBehaviour {

	private Door actualDoor;
	private AudioSource audioCanInteractWithDoor;
	private CanvasManager canvasManager;

	// initialization
	void Start () {
		actualDoor = gameObject.transform.parent.parent.Find("Door").GetComponent<Door> ();
		audioCanInteractWithDoor = GetComponent<AudioSource> ();
		canvasManager = GameObject.Find("Canvas").GetComponent<CanvasManager>() ;
	}

	void OnTriggerEnter(Collider collision)
	{
		if(collision.gameObject.name == "Player") {
			audioCanInteractWithDoor.Play ();
			canvasManager.showOpenDoorButton(actualDoor);
		}
	}

	void OnTriggerStay(Collider collision)
	{
		if(collision.gameObject.name == "Player") {
			if (Input.GetKeyDown ("space")) {
				actualDoor.changeDoorStatus = true;
			}
		}
	}

	void OnTriggerExit(Collider collision)
	{
		if(collision.gameObject.name == "Player") {
			canvasManager.hideOpenDoorButton();
		}
	}

}
