﻿using UnityEngine;
using System.Collections;

public class WinTrigger : MonoBehaviour {

	private GameManager gameManager;

	//game is won if player triggers this event
	void OnTriggerEnter(Collider collision)
	{
		if(collision.gameObject.name == "Player") {
			gameManager.winGame ();
		}
	}

	//initialization
	void Start () {
		gameManager = GameObject.Find("GameManagerObject").GetComponent<GameManager>();
	}
}
