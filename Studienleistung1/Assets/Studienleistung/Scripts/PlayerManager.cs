﻿using UnityEngine;
using System.Collections;

public class PlayerManager : MonoBehaviour {
	
	private int healthPoint = 10;
	private int speed;
	private PlayerInputManager inputController;
	private GameManager gameManager;
	private CanvasManager canvasManager;
	public Vector3 respawnLocation;

	public int changeHealthPoint
	{
		get
		{
			return healthPoint; 
		}

		set
		{
			healthPoint = value;
			changeHealthPointUI ();
			checkHealthPoints ();
		}
	}

	public int changeSpeed
	{
		get
		{
			return speed; 
		}

		set
		{
			speed = value;
			inputController.speedBoost = speed;
		}
	}

	//called iff health Points are changed
	//text for health points will be changed
	private void changeHealthPointUI() {
		canvasManager.changeHealthUI(healthPoint);
	}

	//check health points, if they are equal or less than 0, the game is lost
	private void checkHealthPoints() {
		if (healthPoint <= 0) {
			gameManager.lostGame();
		}
	}

	public void respawnPlayer(){
		inputController.changePlayerLocation (respawnLocation);
	}
		
	// initialization
	public void Start()
	{
		respawnLocation = GetComponent<Rigidbody>().transform.position;
		inputController = GetComponent<PlayerInputManager>();
		gameManager = GameObject.Find("GameManagerObject").GetComponent<GameManager>();
		canvasManager = GameObject.Find("Canvas").GetComponent<CanvasManager>();
		canvasManager.changeHealthUI (healthPoint);
		inputController.Init();
	}
}
