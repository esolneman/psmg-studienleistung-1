﻿using UnityEngine;
using System.Collections;

public class Door : MonoBehaviour {

	public bool isOpen = false;
	public float speed = 1;
	private float doorheight;
	private float currentDoorheight;
	private float doorHeightOpen = 1.5f;
	private AudioSource audioDoorOpen;


	// initialization
	void Start()
	{
		doorheight = transform.position.y;
		audioDoorOpen = GetComponent<AudioSource> ();
	}

	void FixedUpdate() {
		if(isOpen == true) {
			openDoor ();
		}else{
			closeDoor ();
		}
	}

	//if the door is closed, it sink in the ground
	private void openDoor() {
		currentDoorheight = transform.position.y;
		if (currentDoorheight >= doorHeightOpen) {
			audioDoorOpen.Play ();
			transform.Translate(0.0f, -doorheight * Time.deltaTime * speed, 0.0f);
		}
	}

	//if the door is open the door gets back to itsoriginal position
	private void closeDoor() {
		currentDoorheight = transform.position.y;
		if(currentDoorheight < doorheight){
			audioDoorOpen.Play ();
			transform.Translate(0.0f, doorheight * Time.deltaTime * speed, 0.0f);
		}
	}

	public bool changeDoorStatus
	{
		get
		{
			return isOpen; 
		}

		set
		{
			isOpen = value;
		}
	}




}
