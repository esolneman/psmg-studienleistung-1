﻿using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour {

	private GameObject playerObject;
	private PlayerManager playerManager;
	private CanvasManager canvasManager;
	private Vector3 respawnLocation;
	private int defaultHealthValue = 10;

	//initialization
	void Start () {
		playerObject = GameObject.Find("Player");
		playerManager = GameObject.Find("Player").GetComponent<PlayerManager>();
		canvasManager = GameObject.Find("Canvas").GetComponent<CanvasManager>();
		respawnLocation = playerObject.transform.position;
	}

	public void winGame() {
		canvasManager.showWinUI ();
	}

	public void lostGame() {
		canvasManager.showLostUI ();
	}

	//activate all Bombs
	//shows them enable the colliders
	private void activateBombs(){
		BombTrigger [] allBombs;
		allBombs = gameObject.GetComponentsInChildren<BombTrigger> ();
		foreach (BombTrigger currentBomb in allBombs) {
			currentBomb.activateBomb ();
		}
	}

	//close all doors in the game
	private void closeDoors() {
		Door [] allDoors;
		allDoors = gameObject.GetComponentsInChildren<Door> ();
		foreach (Door currentDoor in allDoors) {
			currentDoor.changeDoorStatus = false;
		}
	}

	//reset the time to 0
	private void resetTime(){
		canvasManager.startTimeNew ();
	}

	public void resetGame() {
		activateBombs ();
		closeDoors ();
		resetTime ();
		playerManager.respawnPlayer ();
		playerManager.changeHealthPoint = defaultHealthValue;
	}
}
